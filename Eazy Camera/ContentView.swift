//
//  ContentView.swift
//  Eazy Camera
//
//  Created by Mohan Saravanan on 17/2/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}
