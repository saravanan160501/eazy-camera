//
//  Eazy_CameraApp.swift
//  Eazy Camera
//
//  Created by Mohan Saravanan on 17/2/21.
//

import SwiftUI

@main
struct Eazy_CameraApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
